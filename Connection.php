<?php
error_reporting(E_ERROR | E_PARSE);

/**
 * Created by PhpStorm.
 * User: Anonymous
 * Date: 5/9/18
 * Time: 10:27 PM
 */
class Connection
{

    private $conn;

    public function connect()
    {
        $conn = pg_connect("host=localhost port=5432 dbname=db_country user=postgres password=123");
        if (!$conn)
            die("Could not connect to database!");
        else {
            //echo 'Database connected';
            $this->conn = $conn;
        }
        return $conn;
    }

    public function close()
    {
        pg_close($this->conn);
    }

}