<?php
include_once('Rest.inc.php');
include_once('ApiDataAccess.php');

/**
 * Created by PhpStorm.
 * User: Anonymous
 * Date: 5/9/18
 * Time: 10:34 PM
 */
class ApiController extends REST
{

    public function __construct()
    {
        $request = file_get_contents('php://input');
        $request = json_decode($request, true);

        if ($request['REQ_KEY'] == 'GET_ALL_COUNTRY') {
            new ApiDataAccess($request);

        } else if ($request['REQ_KEY'] == 'GET_A_COUNTRY') {
            new ApiDataAccess($request);

        } else if ($request['REQ_KEY'] == 'ADD_A_COUNTRY') {
            new ApiDataAccess($request);

        } else if ($request['REQ_KEY'] == 'DELETE_A_COUNTRY') {
            new ApiDataAccess($request);

        } else if ($request['REQ_KEY'] == 'UPDATE_A_COUNTRY') {
            new ApiDataAccess($request);

        } else {
            if ($this->get_request_method() != "POST") {
                $this->response('', 406);
            }

            if ($request == null) {
                $error = array(
                    "RES_MSG" => "On Thing - One Direction"
                );
                $this->response($error, 200);

            } else {
                $error = array(
                    "RES_MSG" => "Invalid REQ_KEY"
                );
                $this->response($error, 200);
            }

        }
    }

}

new ApiController();