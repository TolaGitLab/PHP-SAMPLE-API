<?php
include_once('Connection.php');

/**
 * Created by PhpStorm.
 * User: Anonymous
 * Date: 5/9/18
 * Time: 10:52 PM
 */
class ApiDataAccess extends REST
{

    public function __construct($request)
    {
        if ($request['REQ_KEY'] == 'GET_ALL_COUNTRY') {
            $this->GET_ALL_COUNTRY($request);

        } else if ($request['REQ_KEY'] == 'GET_A_COUNTRY') {
            $this->GET_A_COUNTRY($request);

        } else if ($request['REQ_KEY'] == 'ADD_A_COUNTRY') {
            $this->ADD_A_COUNTRY($request);

        } else if ($request['REQ_KEY'] == 'DELETE_A_COUNTRY') {
            $this->DELETE_A_COUNTRY($request);

        } else if ($request['REQ_KEY'] == 'UPDATE_A_COUNTRY') {
            $this->UPDATE_A_COUNTRY($request);

        }
    }

    //GET-ALL-COUNTRIES
    public function GET_ALL_COUNTRY($request)
    {
        if ($this->get_request_method() != 'POST') {
            $this->response('', 406);
        }

        $conn = new Connection();
        $query = 'SELECT * FROM tb_country';

        $resultSet = pg_query($conn->connect(), $query) or die(pg_last_error());
        $conn->close();

        $result = array();

        if (pg_num_rows($resultSet) > 0) {
            while ($row = pg_fetch_array($resultSet, null, PGSQL_ASSOC)) {
                $arrayResult = array(
                    'COUNTRY_ID' => $row['country_id'],
                    'COUNTRY_NAME' => $row['country_name'],
                    'COUNTRY_LAT' => $row['country_lat'],
                    'COUNTRY_LNG' => $row['country_lng'],
                    'DESCRIPTION' => $row['description'],
                );

                $result[] = $arrayResult;
            }

            $success = [
                'REQ_KEY' => $request['REQ_KEY'],
                'RES_MSG' => 'SUCCESSFUL',
                'STATUS' => '1',
                'RES_DATA' => $result
            ];
            $this->response($success, 200);

        } else {
            $failed = [
                'REQ_KEY' => $request['REQ_KEY'],
                'RES_MSG' => 'FAILED',
                'STATUS' => '0'
            ];
            $this->response($failed, 200);
        }

    }

    //GET-A-COUNTRY-BY-ID
    public function GET_A_COUNTRY($request)
    {
        if ($this->get_request_method() != 'POST') {
            $this->response('', 406);
        }

        $data = $request['REQ_DATA'];
        $country_id = $data['COUNTRY_ID'];

        $conn = new Connection();
        $query = "SELECT * FROM tb_country WHERE country_id='" . $country_id . "'";

        $resultSet = pg_query($conn->connect(), $query) or die(pg_last_error());
        $conn->close();

        $result = array();

        if (pg_num_rows($resultSet) > 0) {
            while ($row = pg_fetch_array($resultSet, null, PGSQL_ASSOC)) {
                $arrayResult = array(
                    'COUNTRY_ID' => $row['country_id'],
                    'COUNTRY_NAME' => $row['country_name'],
                    'COUNTRY_LAT' => $row['country_lat'],
                    'COUNTRY_LNG' => $row['country_lng'],
                    'DESCRIPTION' => $row['description'],
                );

                $result = $arrayResult;
            }

            $success = [
                'REQ_KEY' => $request['REQ_KEY'],
                'RES_MSG' => 'SUCCESSFUL',
                'STATUS' => '1',
                'RES_DATA' => $result
            ];
            $this->response($success, 200);

        } else {
            $failed = [
                'REQ_KEY' => $request['REQ_KEY'],
                'RES_MSG' => 'FAILED',
                'STATUS' => '0'
            ];
            $this->response($failed, 200);
        }

    }

    //ADD-NEW-COUNTRY
    public function ADD_A_COUNTRY($request)
    {
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        $data = $request['REQ_DATA'];
        $country_name = $data['COUNTRY_NAME'];
        $country_lat = $data['COUNTRY_LAT'];
        $country_lng = $data['COUNTRY_LNG'];
        $description = $data['DESCRIPTION'];

        $conn = new Connection();
        $query = "INSERT INTO tb_country(country_name, country_lat, country_lng, description) VALUES('" . $country_name . "', '" . $country_lat . "', '" . $country_lng . "', '" . $description . "');";

        $resultSet = pg_query($conn->connect(), $query) or die(pg_last_error());
        $conn->close();

        if (pg_affected_rows($resultSet)) {
            $success = [
                'REQ_KEY' => $request['REQ_KEY'],
                'RES_MSG' => "SUCCESSFUL",
                'STATUS' => '1'
            ];
            $this->response($success, 200);

        } else {
            $failed = [
                'REQ_KEY' => $request['REQ_KEY'],
                'RES_MSG' => "FAILED",
                'STATUS' => '0'
            ];
            $this->response($failed, 200);
        }
    }

    //DELETE-A-COUNTRY
    public function DELETE_A_COUNTRY($request)
    {
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        $data = $request['REQ_DATA'];
        $countryId = $data['COUNTRY_ID'];

        $conn = new Connection();
        $query = "DELETE FROM tb_country WHERE country_id='" . $countryId . "';";

        $resultSet = pg_query($conn->connect(), $query) or die(pg_last_error());
        $conn->close();

        if (pg_affected_rows($resultSet)) {
            $success = [
                'REQ_KEY' => $request['REQ_KEY'],
                'RES_MSG' => "SUCCESSFUL",
                'STATUS' => '1'
            ];
            $this->response($success, 200);

        } else {
            $failed = [
                'REQ_KEY' => $request['REQ_KEY'],
                'RES_MSG' => "FAILED",
                'STATUS' => '0'
            ];
            $this->response($failed, 200);
        }
    }

    //UPDATE-A-COUNTRY
    public function UPDATE_A_COUNTRY($request)
    {
        if ($this->get_request_method() != "POST") {
            $this->response('', 406);
        }

        $data = $request['REQ_DATA'];
        $country_id = $data['COUNTRY_ID'];
        $country_name = $data['COUNTRY_NAME'];
        $country_lat = $data['COUNTRY_LAT'];
        $country_lng = $data['COUNTRY_LNG'];
        $description = $data['DESCRIPTION'];

        $conn = new Connection();
        $query = "UPDATE tb_country SET country_name='" . $country_name . "', country_lat=" . (float)$country_lat . ", country_lng=" . (float)$country_lng . ", description='" . $description . "' WHERE country_id='" . $country_id . "';";

        $resultSet = pg_query($conn->connect(), $query) or die(pg_last_error());
        $conn->close();

        if (pg_affected_rows($resultSet)) {
            $success = [
                'REQ_KEY' => $request['REQ_KEY'],
                'RES_MSG' => "SUCCESSFUL",
                'STATUS' => '1'
            ];
            $this->response($success, 200);

        } else {
            $failed = [
                'REQ_KEY' => $request['REQ_KEY'],
                'RES_MSG' => "FAILED",
                'STATUS' => '0'
            ];
            $this->response($failed, 200);
        }
    }

}