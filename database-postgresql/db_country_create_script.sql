CREATE TABLE tb_country
(
  country_id   SERIAL PRIMARY KEY,
  country_name VARCHAR(200) NOT NULL UNIQUE,
  country_lat  FLOAT,
  country_lng  FLOAT,
  description  TEXT
);

INSERT INTO tb_country (country_name, country_lat, country_lng, description)
VALUES ('Cambodia', 0.1, 0.1, 'Asian Country'),
  ('Myanmar', 0.1, 0.1, 'Asian Country');